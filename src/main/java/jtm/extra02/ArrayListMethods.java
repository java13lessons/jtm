package jtm.extra02;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ArrayListMethods {
	List<Integer> myList = new ArrayList<Integer>();

	//// call the method --> checkArray(2,23,43,12,42,12,19,32,21,10,21);
	public List<Integer> checkArray(int comparator, int... numbers) {
		// TODO #1:Implement method that compares values of passed array with
		// passed comparator.
		// Return list with values that are smaller than comparator.
		// Hint: Investigate how varargs are used.
		for (int i = 0; i < numbers.length; i++)
			if (numbers[i] < comparator)
				myList.add(numbers[i]);

		return myList;
	}

	public int sumResult() {
		int sum = 0;
		// TODO #2: Count element sum of the list
		Iterator<Integer> iterator = this.myList.iterator();
		while (iterator.hasNext())
			sum += iterator.next();

		return sum;
	}
}
