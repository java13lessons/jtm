package jtm.extra06;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegEx {

	/**
	 * This method finds out if we can make lucky number from numbers in input
	 * string. Lucky number is number with digit sum equal to 25
	 * 
	 * @param string , needed to be checked
	 * @return true if numbers in this number are lucky, false if not.
	 */
	public boolean isLuckyNumber(String input) {

		// TODO #1 Remove all non digits from the input.
		// HINT: use negation pattern.

		// TODO #2 count the sum of all digits, and check if the sum is lucky
		Pattern pattern = Pattern.compile("(\\d+)");
		Matcher matcher = pattern.matcher(input);
		Integer sum = 0;
		while (matcher.find())
			sum += Integer.parseInt(matcher.group(1));

		if (sum == 25)
			return true;
		else
			return false;

	}

	/**
	 * This method finds Kenny or Kelly hiding in this list. "Kenny" or "Kelly" can
	 * be written with arbitrary number of "n"s or "l"s starting with two.
	 * 
	 * @param input — input string
	 * @return — position of "Kenny" string starting with zero. If there are no
	 *         "Ken..ny" return -1.
	 */
	public int findKenny(String input) {
		Pattern pattern = Pattern.compile("Kenn+y|Kell+y");//// it means that instead of + there maybe anything
		Matcher mather = pattern.matcher(input);
		if (mather.find())
			return mather.start();
		else
			return -1;

	}

	/**
	 * THis method checks if input string is correct telephone number. Correct Riga
	 * phone number starts with 67 or 66 and is followed by 6 other digits. not
	 * obligate prefix +371
	 * 
	 * @param telephoneNumber - number, needed to be checked.
	 * @return true if number is valid Riga city number.
	 */
	public boolean isGood(String telephoneNumber) {
		if (telephoneNumber.matches("^[+]{0,1}(371)?6(6|7)\\d{6}"))
			return true;
		else
			return false;
	}
}
