package jtm.activity06;

public class Human extends HumanoidClass implements Humanoid {

	private boolean isAlive = true;

	
	@Override
	public void eat(Integer food) {
		// TODO Auto-generated method stub
		super.eat(food);
	}
	
	public Human() {
		this.eaten = 0;
		this.weight = Humanoid.BirthWeight;
	}

	public Human(int weight) {
		this.weight = weight;
	}
	
	@Override
	public int getWeight() {
		// TODO Auto-generated method stub
		return super.getWeight();
	}
	
	@Override
	public String killHimself() {
		this.isAlive = false;
		return this.isAlive();
	}

	@Override
	public Integer vomit() {
		this.weight -= (Integer) this.eaten;
		int previous = (Integer) this.eaten;
		this.eaten = 0;
		return previous;
	}

	@Override
	public String isAlive() {
		if (this.isAlive)
			return "Alive";
		else
			return "Dead";
	}
	
	@Override
	public String toString() {
		return "Human: " + this.weight + " [" + this.eaten + "]";
	}

}
