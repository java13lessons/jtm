package jtm.activity06;

public class HumanoidClass {

	protected int weight;
	protected Object eaten;

	public void eat(Integer food) {

		if (food == null)
			return;

		if (this.eaten instanceof Integer && (Integer) this.eaten > 0)
			return;

		if (!(this.eaten instanceof Integer) && (this.eaten != null))
			return;

		this.weight += food;
		this.eaten = food;

	}
	
	public void setEaten(Object eaten) {
		this.eaten = eaten;
	}
	
	public Object getEaten() {
		return eaten;
	}

	public int getWeight() {
		return this.weight;
	}

}
