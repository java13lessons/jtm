package jtm.activity06;

public class Martian extends HumanoidClass implements Alien, Humanoid, Cloneable {

	public Martian() {
		this.weight = Alien.BirthWeight;
	}

	public Martian(int weight) {
		this.weight = weight;
	}

	@Override
	public void eat(Object item) {
		if (this.eaten != null)
			return;

		this.eaten = item;

		if (item instanceof Humanoid) {
			this.weight += ((Humanoid) item).getWeight();
			((Humanoid) item).killHimself();
		} else if (item instanceof Integer)
			this.weight += ((Integer) item);
	}

	@Override
	public Object vomit() {
		if (this.eaten == null)
			return null;

		if (this.eaten instanceof Humanoid)
			this.weight -= ((Humanoid) this.eaten).getWeight();
		else if (this.eaten instanceof Integer)
			this.weight -= ((Integer) this.eaten);

		Object current = this.eaten;
		this.eaten = null;

		return current;
	}

	@Override
	public int getWeight() {
		// TODO Auto-generated method stub
		return super.getWeight();
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return this.clone(this);
	}

	private Object clone(Object current) {

		if (current instanceof Integer)/// we are trying to clone Integer -- no need to do it
			return current;

		if (current == null)
			return null;

		if (current instanceof Martian) {
			Martian currentMartian = (Martian) current;
			Martian newMartian = new Martian(currentMartian.getWeight());
			newMartian.setEaten(this.clone(currentMartian.getEaten()));//// need to clone the whole content of the
																		//// stomach of the Martian
			return newMartian;
		}

		if (current instanceof Human) {
			Human newHuman = new Human(((Human) current).getWeight());
			newHuman.setEaten(((Human) current).getEaten());
			return newHuman;
		}

		return null;

	}

	@Override
	public void eat(Integer food) {
		super.eat(food);
	}
	
	@Override
	public String isAlive() {
		// TODO Auto-generated method stub
		return Alien.super.isAlive();
	}

	@Override
	public String killHimself() {
		return Alien.super.killHimself();
	}

	@Override
	public String toString() {
		return "Martian: " + this.weight + " [" + this.eaten + "]";
	}

}
