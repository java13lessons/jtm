package jtm.extra10;

import org.apache.log4j.Logger;

public class Log {

	/*- TODO #1
	 * Implement static methods for Log class to write trace, debug, info, warn, error and fatal messages
	 * Note, that this logger should have classpath jtm.extra10.Log
	 */
	private static Logger log = Logger.getLogger(Log.class);

	public static void trace(String message) {
		log.trace(message);
	}

	public static void debug(String message) {
		log.debug(message);
	}

	public static void info(String message) {
		log.info(message);
	}

	public static void warn(String message) {
		log.warn(message);
	}

	public static void error(String message) {
		log.error(message);
	}

	public static void fatal(String message) {
		log.fatal(message);
	}

	/*- TODO #2
	 * Implement public static Logger getLoger() method,
	 * which returns reference to the logger used for logging
	 */

	public static Logger getLogger() {
		return log;
	}

	public static class ExtLog extends Log {
		/*- TODO #2
		 * Implement internal class ExtLog which allows to write into log trace, debug, info, warn, error and fatal messages
		 * Note, that this logger should have classpath jtm.extra10.Log$ExtLog
		 */
		static Logger log = Logger.getLogger(ExtLog.class);

		public static void trace(String message) {
			log.trace(message);
		}

		public static void debug(String message) {
			log.debug(message);
		}

		public static void info(String message) {
			log.info(message);
		}

		public static void warn(String message) {
			log.warn(message);
		}

		public static void error(String message) {
			log.error(message);
		}

		public static void fatal(String message) {
			log.fatal(message);
		}
	}

}
