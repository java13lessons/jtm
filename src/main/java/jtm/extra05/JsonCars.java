package jtm.extra05;

import com.google.gson.*;

import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class JsonCars {

    /*- TODO #1
     * Implement method, which returns list of cars from generated JSON string
     */
    public List<Car> getCars(String jsonString) {
        /*- HINTS:
         * You will need to use:
         * - https://stleary.github.io/JSON-java/org/json/JSONObject.html
         * - https://stleary.github.io/JSON-java/org/json/JSONArray.html
         * You will need to initialize JSON array from "cars" key in JSON string
         */
        JsonParser parser = new JsonParser();
        JsonElement container = parser.parse(jsonString);
        JsonElement carsarray = container.getAsJsonObject().get("cars");
        Car[] cars = new Gson().fromJson(carsarray, Car[].class);

        return Arrays.asList(cars);
    }

    /*- TODO #2
     * Implement method, which returns JSON String generated from list of cars
     */
    public String getJson(List<Car> cars) {
        /*- HINTS:
         * You will need to use:
         * - https://docs.oracle.com/javase/8/docs/api/index.html?java/io/StringWriter.html
         * - http://static.javadoc.io/org.json/json/20180130/index.html?org/json/JSONWriter.html
         * (You could use JSONArray, but tests require entries in specific order, which you
         * can manage with JSONWriter.)
         * Remember to add "car" key as a single container for array of car objects in it.
         */
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Float.class,
                (JsonSerializer<Float>) (src, typeOfSrc, context) -> {
                    DecimalFormat df = new DecimalFormat();
                    df.setGroupingUsed(false);
                    return new JsonPrimitive(Integer.parseInt(df.format(src)));
                });
        Gson gson = builder.create();
        JsonObject obj = new JsonObject();
        obj.add("cars", gson.toJsonTree(cars));

        return obj.toString();
    }

}