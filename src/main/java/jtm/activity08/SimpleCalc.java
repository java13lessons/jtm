package jtm.activity08;

// TODO implement basic mathematical operations with int numbers in range
// of [-10..+10] (including)
// Note that:
// 1. input range is checked using assertions (so if they are disabled, inputs can be any int)
// 2. outputs are always checked and exception is thrown if it is outside proper range

public class SimpleCalc {

	// TODO specify that method can throw SimpleCalcException
	public static int add(int a, int b) throws SimpleCalcException {
		// TODO implement adding operation
		validateInput(a, b);
		return validateOutput(a, b, "+");
	}

	// TODO specify that method can throw SimpleCalcException
	public static int subtract(int a, int b) throws SimpleCalcException {
		// TODO implement subtract operation
		validateInput(a, b);
		return validateOutput(a, b, "-");
	}

	// TODO specify that method can throw SimpleCalcException
	public static int multiply(int a, int b) throws SimpleCalcException {
		// TODO implement multiply operation
		validateInput(a, b);
		return validateOutput(a, b, "*");
	}

	// TODO specify that method can throw SimpleCalcException
	public static int divide(int a, int b) throws SimpleCalcException {
		// TODO implement divide operation
		validateInput(a, b);
		return validateOutput(a, b, "/");
	}

	// TODO: 1 specify that method can throw SimpleCalcException
	// TODO: 2 Validate that inputs are in range of -10..+10 using assertions
	// Use following messages for assertion description if values are not in
	// range:
	// "input value a: A is below -10"
	// "input value a: A is above 10"
	// "input value b: B is below -10"
	// "input value b: B is above 10"
	// "input value a: A is below -10 and b: B is below -10"
	// "input value a: A is above 10 and b: B is below -10"
	// "input value a: a is below -10 and b: B is above 10"
	// "input value a: a is above 10 and b: B is above 10"
	//
	// where: A and B are actual values of a and b.
	//
	// hint:
	// note that assert allows only simple boolean expression
	// (i.e. without &, |, () and similar constructs).
	// therefore for more complicated checks use following approach:
	// if (long && complicated || statement)
	// assert false: "message if statement not fulfilled";

	// TODO: 3 Catch assertion errors and wrap them in SimpleCalcExceptions with
	// message "Assertion error" and caught assertion error as a cause.

	private static String validateElement(int a) {
		if (a < -10)
			return a + " is below -10";
		else if (a > 10)
			return a + " is above 10";
		else
			return "";
	}

	private static void validateInput(int a, int b) throws SimpleCalcException {
		try {
			String validationElement1 = validateElement(a);
			String validationElement2 = validateElement(b);

//			if (validationElement1 != "" && validationElement2 != "")
			assert !(validationElement1 != "" && validationElement2 != "")
					: "input value a: " + validationElement1 + " and b: " + validationElement2;

			assert validationElement1 == "" : "input value a: " + validationElement1;
			assert validationElement2 == "" : "input value b: " + validationElement2;

		} catch (AssertionError er) {
			throw new SimpleCalcException("Assertion error", er);
		}

	}

	// TODO use this method to check that result of operation is also in
	// range of -10..+10.
	// If result is not in range:
	// throw SimpleCalcException with message:
	// "output value a oper b = result is above 10"
	// "output value a oper b = result is below -10"
	// where oper is +, -, *, /
	// Else:
	// return result
	// Hint:
	// If division by zero is performed, catch original exception and create
	// new SimpleCalcException with message "division by zero" and add
	// original division exception as a cause for it.
	private static int validateOutput(int a, int b, String operation) throws SimpleCalcException {
		int result = 0;
		switch (operation) {
		case "+":
			result = a + b;
			break;
		case "-":
			result = a - b;
			break;
		case "*":
			result = a * b;
			break;
		case "/":
			try {
				result = a / b;
			} catch (ArithmeticException e) {
				throw new SimpleCalcException("division by zero", e);
			}
			break;
		}
		if (result < -10)
			throw new SimpleCalcException(
					"output value " + a + " " + operation + " " + b + " = " + result + " is below -10");
		else if (result > 10)
			throw new SimpleCalcException(
					"output value " + a + " " + operation + " " + b + " = " + result + " is above 10");
		return result;
	}
}
