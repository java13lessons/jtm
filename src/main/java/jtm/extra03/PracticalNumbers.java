package jtm.extra03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PracticalNumbers {

	// TODO Read article https://en.wikipedia.org/wiki/Practical_number
	// Implement method, which returns practical numbers in given range
	// including
	public String getPracticalNumbers(int from, int to) {
		List<Integer> practicalNumbers = new ArrayList<Integer>();///// Here we store the practical numbers
		for (int i = from; i <= to; i++) {/// We need to go through all the potential candidates
			int sum = 0;
			/// At first, we need to get all the divisors
			for (int j = 1; j <= i / 2; j++) {//// i/2 is used, because we can't divide by the number, which is greater
												//// than the half of the integer we are checking
				if (i % j == 0)
					if (sum >= j - 1)
						sum += j;
			}
			if (sum >= i - 1)/// if we reached the target (sum == the number we check, then the number is
								/// practical number
				practicalNumbers.add(i);
		}

		return Arrays.toString(practicalNumbers.toArray());
	}

	public static void main(String[] args) {
		PracticalNumbers obj = new PracticalNumbers();
		System.out.println(obj.getPracticalNumbers(1, 460));
	}

}