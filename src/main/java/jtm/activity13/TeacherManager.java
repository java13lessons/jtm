package jtm.activity13;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class TeacherManager {

	protected Connection conn;

	private static Logger log = Logger.getLogger(TeacherManager.class);

	public TeacherManager() {
		// TODO #1 When new TeacherManager is created, create connection to the
		// database server:
		// url =
		// "jdbc:mysql://localhost/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8"
		// user = "student"
		// pass = "Student007"
		// Hints:
		// 1. Do not pass database name into url, because some statements
		// for tests need to be executed server-wise, not just database-wise.
		// 2. Set AutoCommit to false and use conn.commit() where necessary in
		// other methods
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			this.conn = DriverManager.getConnection(
					"jdbc:mysql://127.0.0.1/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8", "root", "");
			this.conn.setAutoCommit(false);
		} catch (Exception e) {
			log.debug(e.getMessage());
		}
	}

	/**
	 * Returns a Teacher instance represented by the specified ID.
	 * 
	 * @param id the ID of teacher
	 * @return a Teacher object
	 */
	public Teacher findTeacher(int id) {
		// TODO #2 Write an sql statement that searches teacher by ID.
		// If teacher is not found return Teacher object with zero or null in
		// its fields!
		// Hint: Because default database is not set in connection,
		// use full notation for table "database_activity.Teacher"

		Teacher teacher = new Teacher(0, null, null);

		try {
			PreparedStatement prpStatement = this.conn
					.prepareStatement("select * from activity13_db.teacher where id = ?");
			prpStatement.setInt(1, id);
			ResultSet rs = prpStatement.executeQuery();
			if (rs.next()) {
				teacher.setId(rs.getInt("id"));
				teacher.setFirstName(rs.getString("firstname"));
				teacher.setLastName(rs.getString("lastname"));
			}
		} catch (SQLException e) {
			log.debug(e.getMessage());
		}

		return teacher;
	}

	/**
	 * Returns a list of Teacher object that contain the specified first name and
	 * last name. This will return an empty List of no match is found.
	 * 
	 * @param firstName the first name of teacher.
	 * @param lastName  the last name of teacher.
	 * @return a list of Teacher object.
	 */
	public List<Teacher> findTeacher(String firstName, String lastName) {
		// TODO #3 Write an sql statement that searches teacher by first and
		// last name and returns results as ArrayList<Teacher>.
		// Note that search results of partial match
		// in form ...like '%value%'... should be returned
		// Note, that if nothing is found return empty list!
		List<Teacher> teachers = new ArrayList<Teacher>();
		try {
			PreparedStatement prpStatement = this.conn
					.prepareStatement("select * from activity13_db.teacher where firstname like ? and lastname like ?");
			prpStatement.setString(1, "%" + firstName + "%");
			prpStatement.setString(2, "%" + lastName + "%");
			ResultSet rs = prpStatement.executeQuery();
			while (rs.next()) {
				Teacher teacher = new Teacher();
				teachers.add(teacher);

				teacher.setId(rs.getInt("id"));
				teacher.setFirstName(rs.getString("firstname"));
				teacher.setLastName(rs.getString("lastname"));
			}
		} catch (SQLException e) {
			log.debug(e.getMessage());
		}

		return teachers;

	}

	/**
	 * Insert an new teacher (first name and last name) into the repository.
	 * 
	 * @param firstName the first name of teacher
	 * @param lastName  the last name of teacher
	 * @return true if success, else false.
	 */

	public boolean insertTeacher(String firstName, String lastName) {
		// TODO #4 Write an sql statement that inserts teacher in database.

		try {
			PreparedStatement prpStatement = this.conn
					.prepareStatement("insert into activity13_db.teacher (firstname,lastname) values (?,?)");
			prpStatement.setString(1, firstName);
			prpStatement.setString(2, lastName);

			int rowsUpdated = prpStatement.executeUpdate();
			if (rowsUpdated > 0) {
				this.conn.commit();
				return true;
			}
		} catch (SQLException e) {
			log.debug(e.getMessage());
		}

		return false;
	}

	/**
	 * Insert teacher object into database
	 * 
	 * @param teacher
	 * @return true on success, false on error (e.g. non-unique id)
	 */
	public boolean insertTeacher(Teacher teacher) {
		// TODO #5 Write an sql statement that inserts teacher in database.

		try {
			PreparedStatement prpStatement = this.conn
					.prepareStatement("insert into activity13_db.teacher (id,firstname,lastname) values (?,?,?)");
			prpStatement.setInt(1, teacher.getId());
			prpStatement.setString(2, teacher.getFirstName());
			prpStatement.setString(3, teacher.getLastName());

			int rowsUpdated = prpStatement.executeUpdate();
			if (rowsUpdated > 0) {
				this.conn.commit();
				return true;
			}
		} catch (SQLException e) {
			log.debug(e.getMessage());
		}

		return false;
	}

	/**
	 * Updates an existing Teacher in the repository with the values represented by
	 * the Teacher object.
	 * 
	 * @param teacher a Teacher object, which contain information for updating.
	 * @return true if row was updated.
	 */
	public boolean updateTeacher(Teacher teacher) {
		boolean status = false;
		// TODO #6 Write an sql statement that updates teacher information.

		try {
			PreparedStatement prpStatement = this.conn
					.prepareStatement("update activity13_db.teacher set firstname = ?, lastname = ? where id = ?");
			prpStatement.setInt(3, teacher.getId());
			prpStatement.setString(1, teacher.getFirstName());
			prpStatement.setString(2, teacher.getLastName());

			int rowsUpdated = prpStatement.executeUpdate();
			if (rowsUpdated > 0) {
				this.conn.commit();
				return true;
			}
		} catch (SQLException e) {
			log.debug(e.getMessage());
		}
		return false;
	}

	/**
	 * Delete an existing Teacher in the repository with the values represented by
	 * the ID.
	 * 
	 * @param id the ID of teacher.
	 * @return true if row was deleted.
	 */
	public boolean deleteTeacher(int id) {
		// TODO #7 Write an sql statement that deletes teacher from database.
		try {
			PreparedStatement prpStatement = this.conn
					.prepareStatement("delete from activity13_db.teacher where id = ?");
			prpStatement.setInt(1, id);

			int rowsUpdated = prpStatement.executeUpdate();
			if (rowsUpdated > 0) {
				this.conn.commit();
				return true;
			}
		} catch (SQLException e) {
			log.debug(e.getMessage());
		}
		return false;
	}

	public void closeConnecion() {
		// TODO Close connection to the database server and reset conn object to null

		try {
			if (this.conn != null) {
				this.conn.close();
				this.conn = null;
			}
		} catch (SQLException e) {
			log.debug(e.getMessage());
		}

	}

}
