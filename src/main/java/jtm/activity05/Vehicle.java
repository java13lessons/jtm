package jtm.activity05;

import jtm.activity04.Road;

public class Vehicle extends jtm.activity04.Transport {

	protected int wheels;

	public Vehicle(String id, float consumption, int tankSize, int wheels) {
		super(id, consumption, tankSize);
		this.wheels = wheels;
	}

	@Override
	public String move(Road road) {
		if (road.getClass() == Road.class) {//////// Since we need to now that the object road belongs to the class Road
											//////// (need to exclude all the subclasses), it is not possible to use
											//////// instanceof operator, because it also include the sublclasses in the
											//////// check
			if (this.canMove(road)) {
				float fuelNeeded = road.getDistance() * (this.getConsumption() / 100);
				float fuelInTank = this.getFuelInTank() - fuelNeeded;
				this.setFuelInTank(fuelInTank);
				return this.getType() + " is driving on " + road + " with " + wheels + " wheels";
			} else
				return this.move(road);
		}
		return "Cannot drive on " + road;
	}

}
