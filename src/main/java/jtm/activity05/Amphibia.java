package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Amphibia extends Transport {

	private byte sails;
	private int wheels;

	public Amphibia(String id, float consumption, int tankSize, byte sails, int wheels) {
		super(id, consumption, tankSize);
		this.sails = sails;
		this.wheels = wheels;
	}

	@Override
	public String move(Road road) {
//		Transport transport;
//		if (road instanceof WaterRoad)
//			transport = new Ship(this.getId(), this.sails);
//		else /// in this case it should not be important if we use else or else if, because
//				/// the Vehicle class will check it
//			transport = new Vehicle(getId(), getConsumption(), getTankSize(), wheels);
//		return transport.move(road);

		if (road.getClass() == Road.class) {//////// Since we need to now that the object road belongs to the class Road
			//////// (need to exclude all the subclasses), it is not possible to use
			//////// instanceof operator, because it also include the sublclasses in the
			//////// check
			if (this.canMove(road)) {
				float fuelNeeded = road.getDistance() * (this.getConsumption() / 100);
				float fuelInTank = this.getFuelInTank() - fuelNeeded;
				this.setFuelInTank(fuelInTank);
				return this.getType() + " is driving on " + road + " with " + wheels + " wheels";
			} else
				return this.move(road);
		} else
			return this.getType() + " is sailing on " + road + " with " + this.sails + " sails";

	}

}
